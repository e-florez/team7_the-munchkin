[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/e-florez%2Fteam7_the-munchkin/Jupyter&ReadMe)

# Do Research Like a Munchkin: 
## Team 7 Mar 2021     

### The Workshop Research Project:           
                                        
Minimize the sum of pair distances for a system of *M* particles 
in an *N*-dimensional space. This project is to approximate the 
smallest sum of pair distances among particles, where distances 
are calculated pairwise and every particle is only 
counted once.

### Documentation
Technical documentation about this project can be accessed [here](https://e-florez.gitlab.io/team7_the-munchkin).

### Installation
1. Install virtual environment:

    ```python3 -m venv venv```

2. Activate virtual environment:

    ```source venv/bin/activate```

3. Install dependencies:

    ```pip3 install -r requirements.txt```

4. Run code:

    ```python3 main.py```

5. Provide an `input` file name 

### Requirements

First you should install the required python packages contained in the file `requirements.txt`.

### Usage

System with 4 particles in a 1-space

```
# four particles system in a one-dimensional space
-10.000
 -2.000
 -1.000
  1.000
```

After running `main.py` for `4particles_1d.dat` you get

1. **Brute Force solution:** finding every permutation of 
   pairs and their corresponding sum of pair distances 
   (for four particles this approach is feasible)
   
   |      Pairing     |sum of pair distance|
   |------------------|:------------------:|
   | [(1, 2), (3, 4)] |         12         |
   | [(1, 3), (2, 4)] |         10         |
   | [(1, 4), (2, 3)] |         12         |
   
   As a result, **the best pairing** is `[(1, 3), (2, 4)]` and 
   the global minimum is **10 units**


2. **Nearest Neighbour Distances:** using *Greedy algorithm* 
   (see [documentations](https://e-florez.gitlab.io/team7_the-munchkin))
   we find the nearest neighbour for certain particle. After pairing them, 
   we start the search again to find the next closest pair and so on.
   In the end, we find the best pairing possible in a less time.

![computational complexity](project_workshop/docs/pics/complexity.svg)

### Contributing
Whenever you wish to change something use your own branch and make sure you have running tests and code as well as user 
documentation before you add a merge request. NEVER merge your own branch.

### Licence

MIT License 

### Authors and Acknowledgment
Main authors are Sara Wirsing (), Julian Heuer (), Edison Florez (_edisonffh@mail.com_), and Gergely Juhasz ()  

### Project Status

In progress