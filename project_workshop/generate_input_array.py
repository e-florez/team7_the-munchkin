import numpy as np
import random


def generate_input_array(num_particles, dim_space, min_distance=1, next_pair=2):
    """
    Summary:
        function to generate coordinates for a `M` particles system
        in an `N`-dimensional space, where every coordinate `(r_i - r_j) = t`
        for all the closest pairs and the next pair is `s` units away.
        As a result, the sum of the closest pairs is `M*t*sqrt(N)`

    Arg:
        num_particles [int]: number of particles
        dim_space [int]: space dimension
        min_distance [int]: min distance for every coordinate between each pair (the closest)
        next_pair [int]: distance to the next pair (by default is twice the min distance)

    returns:
        generated_array [numpy array]: array with  dimension (num_particles, dim_space)
        list_of_closest_pairs [list (int)]: list of the closest pair
    """
    # total number of particles should be even
    try:
        assert (num_particles % 2 == 0), "  total number of particles should be even"
    # the error message provided by the user gets printed
    except AssertionError as e:
        print(e)
        num_particles += 1
        print(f'  New total number of particle: {num_particles}')

    # space dimension must be larger than zero
    try:
        assert (dim_space > 0), "  space dimension must be larger than zero"
    # the error message provided by the user gets printed
    except AssertionError as e:
        print(e)
        dim_space = 1
        print('  space dimension redefine to 1')

    # shortest distance must be less than any other
    try:
        assert (min_distance < next_pair), "  min distance must be less than any other"
    # the error message provided by the user gets printed
    except AssertionError as e:
        print(e)
        # swapping variables
        min_distance, next_pair = next_pair, min_distance
        print(f'  swapping variables new min distance: {min_distance} and new distance to the next pair {next_pair}')

    # shortest distance must be larger than zero
    try:
        assert (min_distance > 0), "  min distance must be larger than zero"
    # the error message provided by the user gets printed
    except AssertionError as e:
        print(e)
        min_distance = 1
        print('  min distance between particles redefined to 1 unit')

    # generating an array (num_particles x dim_space) and randomizing the closest
    # pairs list to avoid the obvious result [(0, 1), (2, 3), (4, 5), (6, 7), ...]

    # fixed seed for testing purposes
    random.seed(123456)

    # randomizing this list of teh closest pairs
    random_list = random.sample(range(num_particles), num_particles)

    # print(random_list)
    list_of_closest_pairs = []

    # initializing the dummy counter for the matrix index
    k = 0

    # starting point to generate all the coordinates
    start = 0

    generated_array = np.zeros((num_particles, dim_space), dtype=int)

    for i in random_list:
        if i % 2 == 0:
            first_coordinate = start + i * (min_distance + next_pair) / 2
        else:
            first_coordinate = start + min_distance * (i + 1) / 2 + next_pair * (i - 1) / 2

        # finding list of the closest pairs, the solution
        list_of_closest_pairs.append(random_list.index(k))

        # replicating the first coordinates for all the others
        for j in range(dim_space):
            generated_array[k][j] = first_coordinate
        k += 1

    if dim_space == 1:
        # to compute the distance matrix from SciPy package
        # we have to add an extra column of zeros
        extra_zeros = np.zeros((num_particles, 1), dtype=int)
        generated_array = np.append(generated_array, extra_zeros, axis=1)

    return generated_array, list_of_closest_pairs


def generate_1d_input_array(num_particles):
    """
    Summary:
        Function to generate a list of 1D  particles. Since many of the function assumes NxD np array inputs,
        we represent these particles in a 2D space.

    Arg:
    num_particles: number of particles

    returns:
    an Nx2 array, first column with the 1D particles and second zeros

    """
    size = 100.

    generated_array = np.zeros((num_particles, 2))
    generated_array[:, 0] = np.random.rand(num_particles) * 100

    return generated_array
