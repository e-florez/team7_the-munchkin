from numpy import loadtxt
from generate_input_array import generate_input_array


def loading_file(file_to_read):
    """
    summary:
        function to load a simple text file to numpy array.

    arg:
        file_to_read [str]: file name to load

    input file structure:
        for M particles in an N-dimensional space we must have a file with M lines/rows
        and N columns and blanks as delimiters (comments after hash '#')

    returns:
        points [numpy array, type=float]: MxN dimensional array
    """
    # checking file existence
    try:
        f = open(file_to_read, 'r')
        f.close()
    except (IOError, EOFError, OSError) as e:
        print(f'\n\t*** ERROR I/O or EOF ***')
        print(f'\t{e}')
        exit()

    # checking file integrity
    try:
        points = loadtxt(file_to_read, comments="#")
    except ValueError as e:
        print(f'\n*** ERROR ***')
        print(f'file: {file_to_read}')
        print(e)

        # Wrong number of columns
        error = str(e)[:-10]
        return error
    else:
        return points


def load_input_array(input_file):
    """
    summary:
        function to load or generate a input array

    arg:
        input_file [str]: input file name to read coordinates

    returns:
        input_array [numpy array]: rows number of particles columns coordinates
        input_message [str]: a string to print out information about input file

    Technical info:
        using module generate_input_array
    """
    if len(input_file):
        # reading input file and transforming into an array
        input_array = loading_file(input_file)

        if 'Wrong number of columns' == str(input_array):
            exit()

        input_message = f"\n{'-' * 50}\n" \
                        f"- Reading Input file\n" \
                        f"{'-' * 50}\n" \
                        f"  input file: {input_file}"

    else:
        # generating a MxN array randomly with a know solution
        print(f'\nGenerating a random array with known solution')
        num_particles = input('   - number of particles [positive even integer]: ')
        dim_space = input('   - space dimension [positive integer]: ')
        print('')

        # checking input parameters
        try:
            assert int(num_particles), "  number of particles must be a positive even integer"
        except AssertionError as e:
            print(e)
            num_particles = 4
            print('  by default the total number of particles is set to FOUR')

        try:
            assert int(dim_space), "  space dimension must be a positive integer"
        except AssertionError as e:
            print(e)
            dim_space = 1
            print('  by default the space dimension is set to ONE')

        # default parameters
        num_particles = int(num_particles)
        dim_space = int(dim_space)
        min_distance = 1
        next_pair = 2

        # known solution M*r*sqrt(N),
        # where M: # num particles, r: min distance, and N: space dimension
        known_closest_sum = 0.5 * num_particles * min_distance * dim_space ** 0.5

        input_array, known_solution_list = generate_input_array(num_particles,
                                                                dim_space,
                                                                min_distance,
                                                                next_pair)

        input_message = f"\n{'-' * 50}\n " \
                        f"- Generating a random input array ({num_particles}x{dim_space})\n" \
                        f"{'-' * 50}\n" \
                        f"  closest pairs are {min_distance} units away\n" \
                        f"  the best pairing is: \n\t{known_solution_list}\n" \
                        f"  minimum sum of the pair distances: {known_closest_sum:.4f} units"

    return input_array, input_message
