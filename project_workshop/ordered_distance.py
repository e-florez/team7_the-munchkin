import numpy as np


def sort_1d(in_particles):
    """
    summary:
        For any np array it orders the rows of the array based in the values in the first columns
    """
    sorted_particles = in_particles[in_particles[:, 0].argsort()]
    return sorted_particles


def ordered_distance(in_particles):
    """
    summary:
        a
    """

    sorted_particles = sort_1d(in_particles)
    n_particles = sorted_particles.shape[0]
    even_particles = sorted_particles[[i for i in range(n_particles) if i % 2 == 0]]
    odd_particles = sorted_particles[[i for i in range(n_particles) if i % 2 != 0]]
    distance = (odd_particles - even_particles)[:, 0].sum()

    # list of index for the sorted pairs
    sorted_index = list(sorted_particles[:, 0])
    particles_index = list(in_particles[:, 0])
    list_of_pairs = []
    for i in sorted_index:
        list_of_pairs.append(particles_index.index(i))

    return distance, list_of_pairs
