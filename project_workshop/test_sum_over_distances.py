import pytest as pytest
import numpy as np
from sum_over_distances import sum_without_double_counting
#from scipy.spatial import distance_matrix
from generate_all_pairs import brute_force_sum
from distance_matrix import distance_matrix
from reading_input import load_input_array

@pytest.mark.parametrize('input_file',
                         [
                             'input1.dat',
                             'input2.dat',
                             'input3.dat'

                         ])
def test_against_brute_force_sum_without_double_counting(input_file):
    """
    summary:
          TEST for a function to compute a sum of distances without double counting,
          compares to values of BruteForce and is therefore quite slow for bigger systems.
    """
    input_array, input_message = load_input_array(input_file)
    distance_array = distance_matrix(input_array)
    min_dist, pair_lists, list_of_dist = brute_force_sum(distance_array, 'True')
    sum_from_dist, list_of_pairs = sum_without_double_counting(distance_array)
    str_list_of_pair = '-'.join(str(s) for r in list_of_pairs for s in r)
    assert sum_from_dist == list_of_dist[str_list_of_pair]




@pytest.mark.parametrize('input_file',
                         [
                             'input1.dat',
                             'input2.dat',
                             'input3.dat'

                         ])
def test_sum_without_double_counting(input_file):
    """
    summary:
          Fast TEST for a function to compute a sum of distances without double counting,
          works currently only for 4 particles in n dimensions.
          The input for every particle has to have the same dimension.
    """
    sum = []
    input_array, input_message = load_input_array(input_file)
    distance_array = distance_matrix(input_array)
    sum.append(distance_array[0][1]+distance_array[2][3])
    sum.append(distance_array[0][2]+distance_array[1][3])
    sum.append(distance_array[0][3]+distance_array[1][2])
    sum_from_dist, list_of_pairs = sum_without_double_counting(distance_array)
    assert sum_from_dist in sum
