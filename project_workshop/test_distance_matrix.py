from distance_matrix import distance_matrix
import numpy as np
import pytest as pytest


@pytest.mark.parametrize('list_of_points, expected_d_matrix',
                         [
                             ([-10, -1, -2, 1], [[0, 9, 8, 11],
                                                 [0, 0, 1, 2],
                                                 [0, 0, 0, 3],
                                                 [0, 0, 0, 0]]),
                             ([[0, 0],
                               [1, 1],
                               [-2, -2]], [[0, 2**0.5, 8**0.5],
                                           [0, 0, 18**0.5],
                                           [0, 0, 0]])
                         ])
def test_distance_matrix(list_of_points, expected_d_matrix):
    """
    summary:
        TEST for a function to compute the distance matrix (upper triangular square matrix)
    """
    d_matrix = distance_matrix(list_of_points)
    assert np.array_equal(d_matrix, expected_d_matrix)
