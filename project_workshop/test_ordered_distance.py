import pytest as pytest
import numpy as np
# from generate_input_array import generate_1d_input_array
from ordered_distance import ordered_distance
from ordered_distance import sort_1d


@pytest.mark.parametrize('in_particles, distance',
                         [
                             (np.array([[1, 0], [0, 0]]), (1., [1, 0]))
                         ])
def test_ordered_distance(in_particles, distance):
    """
    summary:
        The ordered_distance() is implementing the exact solution for the minimal distance calculation for the case of
        1D particles: sorting the particles by their first coordinate than calculate distances between the odd and even
        particles using only their first coordinate.
        * We test if the function gives back the same number of
        * We test if some simples examples

    """

    assert ordered_distance(in_particles) == distance


@pytest.mark.parametrize('in_particles',
                         [
                             (np.array([[1, 3], [0, 2]]))
                         ])
def test_sort_1d(in_particles):
    """
    summary:
        This is a small function returning any numpy array ordered by the values in its first colum from smallest to largest.


    """

    out_particles = sort_1d(in_particles)

    assert in_particles.shape == out_particles.shape
