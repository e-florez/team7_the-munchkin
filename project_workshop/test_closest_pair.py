from closest_pair import closest_pair_brute_force
import numpy as np


def test_closest_pair():
    """
    list of points:
            x0 = -10 ; x1 = -1 ; x2 = -2 ; x3 = 1

    Closest pair: (x1, x2)
    """
    list_of_point = [-10, -1, -2, 1]
    assert closest_pair_brute_force(list_of_point) == (1, 2)

def test_2D():
    """
    list of 2D points
    """
    list_of_point = [(-10, -1), (-2, 1)]
    assert closest_pair_brute_force(list_of_point) == (0, 1)
