import pytest as pytest
import numpy as np
from generate_all_pairs import generate_all_pairs, brute_force_sum
from distance_matrix import distance_matrix


@pytest.mark.parametrize('num_particles, expected_all_pairs',
                         [
                             (4, [[1, 2, 3, 4],
                                  [1, 3, 2, 4],
                                  [1, 4, 2, 3]]),
                             (6, [[1, 2, 3, 4, 5, 6],
                                  [1, 2, 3, 5, 4, 6],
                                  [1, 2, 3, 6, 4, 5],
                                  [1, 3, 2, 4, 5, 6],
                                  [1, 3, 2, 5, 4, 6],
                                  [1, 3, 2, 6, 4, 5],
                                  [1, 4, 2, 3, 5, 6],
                                  [1, 4, 2, 5, 3, 6],
                                  [1, 4, 2, 6, 3, 5],
                                  [1, 5, 2, 3, 4, 6],
                                  [1, 5, 2, 4, 3, 6],
                                  [1, 5, 2, 6, 3, 4],
                                  [1, 6, 2, 3, 4, 5],
                                  [1, 6, 2, 4, 3, 5],
                                  [1, 6, 2, 5, 3, 4]])
                         ])
def test_generate_all_pairs(num_particles, expected_all_pairs):
    """
    summary:
        Test for the function to generate all the possible pairs (k) for
        a system of N particles (N must be even).

                K = n!/(2**(n/2)*(n/2)!)
    """
    all_pairs = list()

    # default list [1, 2, 3, 4,...], natural order
    default_list_of_particles = list(range(1, num_particles + 1))

    for x in generate_all_pairs(default_list_of_particles):
        all_pairs.append(x)

    assert np.array_equal(all_pairs, expected_all_pairs)


@pytest.mark.parametrize('input_particles, expected_min_sum, expected_min_sum_list_pairs, '
                         'expected_sum_of_all_possible_pairs',
                         [
                             ([-10, -1, -2, 1], 10, '0-2-1-3', {'0-1-2-3': 12,
                                                                '0-2-1-3': 10,
                                                                '0-3-1-2': 12})
                         ])
def test_sum_all_pairs(input_particles, expected_min_sum, expected_min_sum_list_pairs,
                       expected_sum_of_all_possible_pairs):
    """
    summary:
        Test for a function to create a dictionary with every possible combination of pair
        as keys and the the total sum of their distances as values

    """
    d_matrix = distance_matrix(input_particles)

    min_sum, min_sum_list_pairs, sum_of_all_possible_pairs = brute_force_sum(d_matrix, create_dict=True)

    assert np.array_equal(sum_of_all_possible_pairs, expected_sum_of_all_possible_pairs)
    assert min_sum == expected_min_sum
    assert min_sum_list_pairs == expected_min_sum_list_pairs
