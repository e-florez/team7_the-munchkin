import sys
import time
import numpy as np
from reading_input import load_input_array
from distance_matrix import distance_matrix
from sum_over_distances import sum_without_double_counting
from generate_all_pairs import brute_force_sum
from ordered_distance import ordered_distance

# Entry point
if __name__ == '__main__':
    # -----------------------------------------------------------------
    # general setting and format
    # -----------------------------------------------------------------
    # header
    header_message = f"{'*' * 50}\n" \
                     f"*{'Do Research Like a Munchkin (Mar 2021)':^48}*\n" \
                     f"*{'The Workshop Research Project':^48}*\n" \
                     f"*{'':^48}*\n" \
                     f"*{' Minimize sum of pair distances for a system':48}*\n" \
                     f"*{' of M particles in a N-dimensional space':48}*\n" \
                     f"*{'':^48}*\n" \
                     f"*{' by:':48}*\n" \
                     f"*{'   Sara Wirsing (),':48}*\n" \
                     f"*{'   Julian Heuer (),':48}*\n" \
                     f"*{'   Edison Florez (edisonffh@mail.com),':48}*\n" \
                     f"*{'   Gergely Juhasz ()':48}*\n" \
                     f"{'*' * 50}"

    # format to print out numpy arrays
    np.set_printoptions(precision=3, suppress=True)

    general_time_start = time.time()

    # -----------------------------------------------------------------
    # reading input file
    # -----------------------------------------------------------------
    input_file = input(f'\ninput file name with particle coordinates: ')

    # -----------------------------------------------------------------
    # Generating input array
    # -----------------------------------------------------------------
    input_array, input_message = load_input_array(input_file)

    num_particles = input_array.shape[0]

    try:
        dim_space = input_array.shape[1]
    except IndexError:
        # for a one dimensional space
        dim_space = 1

    # -----------------------------------------------------------------
    # OUTPUT: Redirecting the Standard Output Stream
    # -----------------------------------------------------------------
    # Save a reference to the original standard output
    original_stdout = sys.stdout

    file_out = open('min_sum_pair_distance.out', 'w')

    # Change the standard output to the file we created.
    sys.stdout = file_out

    # - printing header
    print(header_message)
    print(input_message)

    # -----------------------------------------------------------------
    # Coordinates
    # -----------------------------------------------------------------
    show_arrays = True
    if show_arrays:
        # showing the array
        print('')
        print(f'-' * 50)
        print(f'- Coordinates')
        print(f'-' * 50)
        print(f'  system of {num_particles} particles in a {dim_space} dimensional space\n')
        print(input_array)

    # 1D: to compute the distance matrix from SciPy package
    # we have to add an extra column of zeros
    if dim_space == 1:
        input_array = np.array([input_array])
        input_array = input_array.T

        extra_zeros = np.zeros((num_particles, 1), dtype=int)
        input_array = np.append(input_array, extra_zeros, axis=1)

    # -----------------------------------------------------------------
    # computing distance matrix
    # -----------------------------------------------------------------
    d_matrix_time_start = time.time()
    d_matrix = distance_matrix(input_array)

    if show_arrays:
        # showing the distance matrix
        print('')
        print(f'-' * 50)
        print(f'- Distance matrix')
        print(f'-' * 50)
        print(d_matrix)

    d_matrix_time_end = time.time()
    print(f'\n  Time computing the distance matrix: '
          f'{d_matrix_time_end - d_matrix_time_start:.3e} sec')

    # -----------------------------------------------------------------
    # closest neighbour sum approach
    # -----------------------------------------------------------------
    closest_neighbour_time_start = time.time()
    if dim_space == 1:
        closest_sum, min_sum_list_pairs = ordered_distance(input_array)
    else:
        closest_sum, min_sum_list_pairs = sum_without_double_counting(d_matrix)

    closest_neighbour_time_end = time.time()
    print('')
    print(f'-' * 50)
    print(f'- Pairing closest neighbour')
    print(f'-' * 50)
    print(f'  Minimum sum of the pair distances: {closest_sum:.4f}')
    print(f'  for this pairs: {min_sum_list_pairs}')

    print(f'\n  Time computing closest neighbour approach: '
          f'{closest_neighbour_time_end - closest_neighbour_time_start:.2e} sec')

    # -----------------------------------------------------------------
    # - brute force approach
    # -----------------------------------------------------------------
    brute_force_time_start = time.time()

    print('')
    print(f'-' * 50)
    print(f'- Brute force Approach')
    print(f'-' * 50)

    # dictionary 'sum_of_all_possible_pairs' is only created by request, it could be too large in size
    create_dict = False

    min_sum_of_pairs, min_sum_list_pairs, sum_of_all_possible_pairs = brute_force_sum(d_matrix,
                                                                                      create_dict)

    print(f'  global minimum sum of the pair distances: {min_sum_of_pairs:.4f}')
    print(f'  for this pairs: {min_sum_list_pairs}')

    if create_dict:
        print(f'\n  All possible pairs: ')
        for key in sum_of_all_possible_pairs:
            print(f'\t{key} --> sum distance: {sum_of_all_possible_pairs[key]:.4f}')

    brute_force_time_end = time.time()
    print(f'\n  Time computing brute force approach: '
          f'{brute_force_time_end - brute_force_time_start:.2e} sec')

    # -----------------------------------------------------------------
    # END of Execution
    # -----------------------------------------------------------------
    general_time_end = time.time()
    print('')
    print('*' * 50)
    print(f'END of Execution')
    print(f'elapse time: {general_time_end - general_time_start:.3e} sec')
    print(f'*' * 50)

    # -----------------------------------------------------------------
    # Reset the standard output to its original value
    sys.stdout = original_stdout
    file_out.close()

    exit()
# -----------------------------------------------------------------
