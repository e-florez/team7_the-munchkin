import numpy as np


def distance(point1, point2):
    """calculate euclidean norm, needs proper data type."""
    arr1 = np.array(point1)
    arr2 = np.array(point2)
    return np.linalg.norm(arr1 - arr2)


def smallest_number(list_of_numbers):
    """finds minimum of a list of numbers. Works only for real numbers,
    but distances should be real and positive anyway"""
    return min(list_of_numbers)


def closest_pair_brute_force(list_of_points, list_of_distances):
    """SUMMARY:
    exhaustive search function to find the closest pair
    for a set of points in several dimensions and complex space.
    The closest pair of points can be computed in O(n2)
    time by performing a brute-force search.
    To do that, one could compute the distances between all
    the n(n−1)/2 pairs of points, then pick the pair with
    the smallest distance.
    Additionally a list of distances between all pairs of points is
    initialized for later use.
    """
    # - initialization, min value a very large number
    min_dist = float('inf')
    dist_ij = 0
    closest = 0
    for i in range(len(list_of_points) - 1):
        for j in range(i + 1, len(list_of_points)):
            dist_ij = distance(list_of_points[i], list_of_points[j])
            list_of_distances.append(dist_ij)
            if dist_ij < min_dist:
                min_dist = dist_ij
                closest = (i, j)

    return closest


def test_calculate_sum_of_distances():
    pass
