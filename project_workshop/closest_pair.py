import numpy as np


def closest_pair_brute_force(list_of_points):
    """SUMMARY:
    exhaustive search function to find the closest pair
    for a set of points in ONE DIMENSION (1D).
    The closest pair of points can be computed in O(n2)
    time by performing a brute-force search.
    To do that, one could compute the distances between all
    the n(n−1)/2 pairs of points, then pick the pair with
    the smallest distance
    """

    closest = (0, 0)
    # - initialization, min value a very large number
    min_dist = float('inf')

    # - two loop (two indexes) to compute the distance between two points
    for i in range(len(list_of_points) - 1):
        for j in range(i + 1, len(list_of_points)):
            p1 = np.array(list_of_points[i])
            p2 = np.array(list_of_points[j])

            # - computing the distance between two points in 1D
            distance = np.linalg.norm(p1 - p2)

            if distance < min_dist:
                min_dist = distance
                closest = (i, j)

    return closest
