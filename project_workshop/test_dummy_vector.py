import pytest as pytest
from dummy_vector import dummy_vector
import scipy.spatial as spatial


@pytest.mark.parametrize('npoints, ndimensions',
                         [
                             (4, 6),
                             (8, 2),
                             (20, 15)

                         ])
def test_dummy_vector_numbers(npoints, ndimensions):
    """
    Testing dummy_vector()
    INPUT  npoints  (int)
           ndimensions (int)
    OUTPUT: numpy array
    Basic test if the narray is indeed the expected size

    check if dim 2
    check if len() and len() ==
    """
    is_dim = (dummy_vector(npoints, ndimensions).ndim == 2)
    is_shape = (dummy_vector(npoints, ndimensions).shape == (npoints, ndimensions))
    assert is_dim and is_shape


@pytest.mark.parametrize('npoints, ndimensions',
                         [
                             (4, 6),
                             (8, 2),
                             (20, 15)
                         ])
def test_dummy_vector_distance(npoints, ndimensions):
    """
    Testing if indeed the distance between 2 point are 1.0  if they are in pairs (2n and 2n+1) 
    and much larger if they are not in pairs.
    """

    dummy = dummy_vector(npoints, ndimensions)
    npairs = int(npoints / 2)

    # This logical variable indicates if the distances between the member of pairs OK
    check_pairs = True
    for pair in range(npairs):
        if spatial.distance.pdist([dummy[2*pair, :], dummy[2*pair+1, :]]) != 1.:
            check_pairs = False

    assert check_pairs
