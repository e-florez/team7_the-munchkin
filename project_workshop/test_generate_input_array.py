import pytest as pytest
import numpy as np
from generate_input_array import generate_input_array
from generate_input_array import generate_1d_input_array
from distance_matrix import distance_matrix


@pytest.mark.parametrize('num_particles, dim_space, min_distance, next_pair, expected_array_dimension',
                         [
                             (4, 2, 1, 2, (4, 2)),
                             (10, 7, 3, 5, (10, 7))
                         ])
def test_generate_input_array_dim(num_particles, dim_space, min_distance, next_pair, expected_array_dimension):
    """
    summary:
          Testing array dimension for M particles in an N-dimensional space
          must generate an array MxN
    """
    array_generated, list_of_pairs = generate_input_array(num_particles, dim_space, min_distance, next_pair)
    dimension = array_generated.shape

    assert np.array_equal(dimension, expected_array_dimension)


@pytest.mark.parametrize('num_particles, dim_space, min_distance, next_pair, expected_list_pairs',
                         [
                             (4, 2, 1, 2, [1, 3, 0, 2]),
                             (10, 7, 3, 5, [1, 7, 2, 8, 0, 4, 5, 9, 3, 6])
                         ])
def test_generate_input_array_distances(num_particles, dim_space, min_distance, next_pair, expected_list_pairs):
    """
    summary:
          Testing distance among particles in the closest pairs must be equal to `min_distance`

    Warning:
        SEED should be FIXED to 123456 for testing purposes
    """
    array_generated, list_of_pairs = generate_input_array(num_particles, dim_space, min_distance, next_pair)

    assert np.array_equal(list_of_pairs, expected_list_pairs)

    d_matrix = distance_matrix(array_generated)

    # according the Euclidean distance for the closest pair at `r` units
    min_euclidean = min_distance * dim_space ** 0.5

    closest_pair_distance = False
    for i in range(0, len(list_of_pairs), 2):
        first_particle = list_of_pairs[i]
        second_particle = list_of_pairs[i + 1]

        if (d_matrix[first_particle][second_particle] - min_euclidean) < 1e-6:
            closest_pair_distance = True

    assert closest_pair_distance


@pytest.mark.parametrize('num_particles',
                         [
                             (4),
                             (18)
                         ])
def test_generate_1d_input_array(num_particles):
    """
    We expect an Nx2 array, where N is even number and the second columns are just zeros.
    I haven't check the zeros yet

    """
    if (num_particles % 2 == 0):
        new_array = generate_1d_input_array(num_particles)

    new_particples, dimensions = new_array.shape

    assert (new_particples == num_particles) and (new_particples % 2 == 0 ) and (dimensions == 2)

