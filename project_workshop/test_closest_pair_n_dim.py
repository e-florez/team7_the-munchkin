from closest_pair_n_dim import distance, smallest_number, closest_pair_brute_force
import pytest as pytest
import numpy as np


@pytest.mark.parametrize('point1, point2, expected_distance',
                         [
                             (1, 10, 9),
                             (-2, 5, 7),
                             ([1 + 1j], 1, 1),
                             ([1, 2], [5, 2], 4)
                         ])
def tests_distance(point1, point2, expected_distance):
    """tests calculation of distance for different kind of data sets
    (1D, positive and negative numbers, complex numbers, 2-dimensional)"""
    assert distance(point1, point2) == expected_distance


@pytest.mark.parametrize('list_of_numbers, smallest',
                         [
                             ([1, 10, 9], 1),
                             ([0, 0.11111111111111111, 0.111111111111111111], 0),
                             ([np.e, np.pi, 2 ** 0.5], 2 ** 0.5)
                         ])
def test_smallest_number(list_of_numbers, smallest):
    """tests for finding the smallest number in a list.
    In the current application this need only work for positive (including 0), real numbers"""
    assert smallest_number(list_of_numbers) == smallest


@pytest.mark.parametrize('list_of_points, closest_pair',
                         [
                             ([-10, -1, -2, 1], (1, 2)),
                             ([[-10, 2], [-1, 0], [-2, 0], [1, 0]], (1, 2)),
                             ([[-10, 2], [-1], [-2], [1, 0]], (1, 2)),
                             ([[-10, 2], [-1j], [-2], [1, 0]], (1, 3))
                         ])
def test_closest_pair_brute_force(list_of_points, closest_pair):
    """"tests for finding the closest pair of points in a number of points"""
    assert closest_pair_brute_force(list_of_points, []) == closest_pair
