#!/usr/bin/python3.8

import numpy as np
from math import factorial, gamma
from bokeh.plotting import figure, show, output_file
from bokeh.palettes import Category20
from bokeh.models import HoverTool
from itertools import cycle

# importing range1d from
# bokeh.models in order to change
# the X-Axis and Y-Axis ranges
from bokeh.models import Range1d

# -----------------------------------------------------------
p = figure()
p.sizing_mode = 'stretch_both'

# - bokeh output file
output_file('view_plot.html')

x = np.arange(0, 9, 0.01)

# brute force
brute_force = []
for i in x:
    # k = factorial(i)
    k = gamma(i + 1)
    brute_force.append(int(k))

# ---------------------------------------------------------
# Plot
p.line(x, brute_force,
       legend_label='Brute Force',
       color='red',
       line_width=3,
       name='brute force'
       )

p.line(x, x ** 2,
       legend_label='Nearest Neighbour Distances (NND)',
       color='blue',
       line_width=3,
       name='NND'
       )

p.line(x, x ** 3,
       legend_label='Greedy Search',
       color='black',
       line_width=3,
       name='greedy'
       )

p.add_tools(HoverTool(tooltips=[('(x, y)', '(@x, @y)'),
                                ('file', '$name')]
                      ))

# ---------------------------------------------------------
# - Ending the plot
p.xaxis.axis_label = 'Number of particles (n)'
p.yaxis.axis_label = 'Computational Complexity O(n)'

# p.x_range = Range1d(0, 10)
p.y_range = Range1d(0, 2000)

p.legend.location = "top_left"
p.legend.label_text_font_size = "16pt"

p.xaxis.major_label_text_font_size = '16px'
p.yaxis.major_label_text_font_size = '16px'

p.xaxis.axis_label_text_font_size = "20pt"
p.yaxis.axis_label_text_font_size = "20pt"

# Click to hide/show lines
p.legend.click_policy = 'hide'
p.output_backend = "svg"

show(p)
# ---------------------------- END
print(f'\n *** DONE ***\n')
# ---------------------------- END
exit()
