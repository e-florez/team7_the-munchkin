from math import factorial


def generate_all_pairs(particle_index):
    """
    summary:
        Recursive function to generate the K possible pair from a set of
        N particles (N must be even), where
            K = n!/(2**(n/2)*(n/2)!)
    arg:
        particles_index [list of int]: number of particles from 1 to N, natural sorted

    return:
        list_of_index [int list, len: num_particles]: list
    """

    if len(particle_index) < 2:
        yield []
        return
    if len(particle_index) % 2 == 1:
        # Handle odd length list
        for i in range(len(particle_index)):
            for result in generate_all_pairs(particle_index[:i] + particle_index[i + 1:]):
                yield result
    else:
        a = particle_index[0]

        for i in range(1, len(particle_index)):
            pair = [a, particle_index[i]]
            for rest in generate_all_pairs(particle_index[1:i] + particle_index[i + 1:]):
                yield pair + rest


def sum_of_pair_distances(list_of_index, d_matrix):
    """
    summary:
        function to add values for a given matrix according to a list
        of index with EVEN number of elements

    arg:
        list_of_index [int list]: list of index with EVEN length

    returns:
        sum_dist [float]: a total sum of elements for a matrix
        pairing them from a list of index
    """
    sum_dist = 0
    for k in range(0, len(list_of_index), 2):
        i = list_of_index[k]
        j = list_of_index[k + 1]
        sum_dist += d_matrix[i][j]

    return sum_dist


def brute_force_sum(d_matrix, create_dict=False):
    """
    summary:
        function to sum distances for all pairs possible for
        N particles (N must be an even LESS or equal to 20),

                    K = n!/(2**(n/2)*(n/2)!)

        where K is the total number of a different way to pair N (=n) particles.

    Warning!: K ~ 650 millions for 20 particles

    arg:
        d_matrix [NxN array]: distance matrix for a system of N particles
        create_dict [boolean]: dictionary is only created by request, it could be too large in size

    return:
        sum_of_all_possible_pairs [dict]: a dictionary where keys are particle index paired
        and their total sum of each pair are the values
        min_sum_of_pairs [float]: global min for the sum of pair distances
    """
    num_particles = d_matrix.shape[0]

    # natural sorted list of N particles, from 1 to N
    default_list_of_particles = list(range(0, num_particles))

    n = len(default_list_of_particles)
    k = factorial(n) / (2 ** (n / 2) * factorial(n / 2))
    k = int(k)

    print(f'  for a system of {len(default_list_of_particles)} particles')
    print(f'  there are {k:.2e} different ways to pair them')

    sum_of_all_possible_pairs = {}
    min_sum_of_pairs = float('inf')

    # this dict is only created by request, it could be too large in size
    if create_dict:
        for list_of_pairs in generate_all_pairs(default_list_of_particles):
            sum_of_distance = sum_of_pair_distances(list_of_pairs, d_matrix)

            # creating a dict for list of index and their sum of distance
            str_list_of_pair = '-'.join(str(s) for s in list_of_pairs)
            sum_of_all_possible_pairs[str_list_of_pair] = sum_of_distance

            if sum_of_distance < min_sum_of_pairs:
                min_sum_of_pairs = sum_of_distance
                min_sum_list_pairs = str_list_of_pair

    # only computing the minimum
    else:
        for list_of_pairs in generate_all_pairs(default_list_of_particles):
            sum_of_distance = sum_of_pair_distances(list_of_pairs, d_matrix)

            str_list_of_pair = '-'.join(str(s) for s in list_of_pairs)

            if sum_of_distance < min_sum_of_pairs:
                min_sum_of_pairs = sum_of_distance
                min_sum_list_pairs = str_list_of_pair

    return min_sum_of_pairs, min_sum_list_pairs, sum_of_all_possible_pairs
