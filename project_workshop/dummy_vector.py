import numpy as np


# import scipy.spatial as spatial


def dummy_vector(npoints, ndimensions):
    """
    Creating an array of particles / points in pairs along two vectors: big_vector, small_
    INPUT:
    npoints:  number of points to generate
    ndimensions: the dimension on the space we are in

    OUTPUT:    (np array)
    every line is a particle/point
    every column is a dimension
    example: dummy_vector[0,:] is the coordinates of the first particle
    """
    # creating the two vectors

    big_step = 5.
    small_step = 1.

    small_vector = np.zeros(ndimensions)
    small_vector[0] = small_step
    big_vector = np.ones(ndimensions) * big_step

    # creating an empty (all 0) np array to fill up with the coordinates later
    points = np.zeros([npoints, ndimensions])
    npairs = int(npoints / 2)

    # This logical variable indicates if the distances between the member of pairs OK
    for pair in range(npairs):
        points[2 * pair, :] = pair * big_vector
        points[2 * pair + 1, :] = pair * big_vector + small_vector

    # print(points)

    return points

# print(dummy_vector(6, 3))
