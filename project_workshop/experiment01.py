import time


def check_file(file_to_read):
    """
    Checking input file before feeding to the the calculations

    arg:
        file_to_read [str]: file name to load
    returns:
        True if OK
        some error is not OK
    """
    from numpy import loadtxt

    valid = False

    try:
        points = loadtxt(file_to_read, comments="#")
    except ValueError as e:
        print(e)
        # Wrong number of columns
        error = str(e)[:-10]
        return error
    else:
        valid = True

    return valid

def experiment01(input_file):
    """
    This is a function that ties together the necessarily steps for an experiment and
    gives back the solution + timing
    Ideally, the calculation would be a separate file that takes array as an input

    It can be called as a function (so several can be compared) or through __main__ for demo

    """
    #from distance_matrix import distance_matrix
    from sum_over_distances import sum_without_double_counting



    # Find pairs / calculate ideal distance
    tic = time.perf_counter()
    #distances = distance_matrix(particle_Array)
    first_solution = sum_without_double_counting(input_file)
    toc = time.perf_counter()

    runtime = toc - tic
    return runtime, first_solution

if __name__ == "__main__":
    """
    If called as a script, it executes experiment01 with a default input file
    It can be called as a function with any input vector, too   
    """
    #import sys
    #from reading_input import reading_file

    input_file = "input2.dat"

    #if check_file(input_file):
    #    particles = reading_file(input_file)
    #else:
    #    sys.exit()

    runtime = 0.
    if check_file(input_file):
        runtime, solution = experiment01(input_file)
    print(f"The experiment with input1.dat takes  {runtime:0.5f} seconds")
