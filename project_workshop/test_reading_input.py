from reading_input import loading_file
import pytest as pytest
import numpy as np


@pytest.mark.parametrize('input_file, list_of_points',
                         [
                             ('input1.dat', [-10, -1, -2, 1]),
                             ('input2.dat', [[-10, -10],
                                             [-1, -1],
                                             [-2, -2],
                                             [1, 1]]),
                             ('input3.dat', [[-10, -10, -10],
                                             [-1, -1, -1],
                                             [-2, -2, -2],
                                             [1, 1, 1]]),
                             ('input3_0.dat', 'Wrong number of columns'),
                             ('input3_1.dat', 'Wrong number of columns'),
                         ])
def test_reading_input(input_file, list_of_points):
    """
    summary:
          TEST for a function to read input file
    """
    input_points = loading_file(input_file)
    assert np.array_equal(input_points, list_of_points)
