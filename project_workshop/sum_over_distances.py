import pytest as pytest
import numpy as np
import copy
#from reading_input import reading_file
from scipy.spatial import distance_matrix


"""should implement sanity checks:"""
"""even number of particles and zeros on diagonal of distance matrix"""

"""At this point, this function calculates a sum over distances without double counting,
starting from the lowest distance in the array, deleting all elements of the used particles,
than adding the next smallest distance and so on.
It basically works as long as all particles in the input have the same dimension > 1 (due to use of
distance matrix function). The tests however only test cases up to four particles."""


def sum_without_double_counting(input_file):
    sum = 0
    distance_array = copy.deepcopy(input_file)
    min_i = 0
    min_j = 0
    k = 0
    list_of_pairs = []
    number_of_rows = distance_array.shape[0]
    while k < number_of_rows*0.5:
        min_dist = float('inf')
        for i in range(0, distance_array.shape[0]):
            for j in (j for j in range(i + 1, distance_array.shape[0]) if j != i):
                if distance_array[i, j] < min_dist:
                    min_dist = distance_array[i, j]
                    min_i = i
                    min_j = j
        list_of_pairs.append([min_i, min_j])
        sum += min_dist
        inf_array = np.full(number_of_rows, float('inf'))
        distance_array[min_i] = inf_array
        distance_array[min_j] = inf_array
        distance_array[:,min_i] = inf_array
        distance_array[:,min_j] = inf_array
        k += 1
    sorted_list_of_pairs = sorted(list_of_pairs, key = lambda x: x[0])
    return sum, sorted_list_of_pairs
