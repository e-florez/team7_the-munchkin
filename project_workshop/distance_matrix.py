import numpy as np


def distance_matrix(input_list_of_points):
    """
    summary:
        function to compute distance matrix for a M particles system in a N-dimensional space,
        according to the Euclidean distance; i.e.,

            d_ij = sqrt((p1_i - p1_j)**2 + (p2_i - p2_j)**2 + ... + (pn_i - pn_j)**2)
            where p belong to R^n

    arg:
        list_of_points [array]: MxN dimensional numpy array (M: num particles, N: space dimension)

    returns:
        a UPPER TRIANGULAR matrix (MxM, for a M particles system) and the main diagonal are zero; i.e.,
        d_ij = 0 for each i>=j
    """
    dimension = len(input_list_of_points)
    d_matrix = np.zeros((dimension, dimension), dtype=float)

    # - two loop (two indexes) to compute the distance between two points
    for i in range(dimension - 1):
        for j in range(i + 1, dimension):
            p1 = np.array(input_list_of_points[i])
            p2 = np.array(input_list_of_points[j])

            # - computing the matrix or vector norm between two points
            distance = np.linalg.norm(p1 - p2)

            d_matrix[i, j] = distance
            # d_matrix[j, i] = distance

    return d_matrix
