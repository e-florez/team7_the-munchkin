# Minimize The Sum of Pair Distances

Minimize the sum of pair distances for a system of *M* particles
in an *N*-dimensional space. This project is to approximate the 
smallest sum of pair distances among particles, where distances 
are calculated pairwise and every particle is only counted once.

The problem is similar to the shortest path through a fully 
connected network (mesh), where only is allowed to visit each 
vertex or node once. 

It is a simpler version of the **Travelling Salesman Problem** (NP-hard)
for a weighted undirected fully connected graph, defined as *G=(M, M^2)*, 
consisting of the set of *M* nodes (particles) and the set of *M^2* 
edges (fully connected) where the weight is the Euclidean distance. 

## Research problem

Given an *M* number of particles (even), find the way to group them
in pairs so that the total sum of pair distances is minimized.

The Euclidean distance for any pair of particles in an *N*-dimensional
space is

![\large d(p, q)=\sqrt{\sum_a^{N}{(p_a-q_a)^2}}](https://latex.codecogs.com/svg.latex?\large&space;d(p,&space;q)=\sqrt{\sum_a^{N}{(p_a-q_a)^2}})

where **_p_**, **_q_** are two points in Euclidean N-space. 
Thus, the target function is

![min\left(\sum_{p\neq q}^M{d(p, q)}\right)](https://latex.codecogs.com/svg.latex?\large&space;min\left(\sum_{p\neq&space;q}^M{d(p,&space;q)}\right))

###Example 

four particles in one dimensional space:

 - p1: -10 
 - p2: -1
 - p3: -2
 - p4: 1

We can write permutation of pairs and their corresponding target function

|        Pairing       |Target function|
|----------------------|:-------------:|
| [(p1, p2), (p3, p4)] |       12      |
| [(p1, p3), (p2, p4)] |       10      |
| [(p1, p4), (p2, p3)] |       12      |

As a result, **the best pairing** is `[(p1, p3), (p2, p4)]` and 
the global minimum is **10 units**

This result correspond to the shortest path (colored) for a system of four
particles in a fully connected, weighted, and undirected graph
*G(4, 16)*.

![graph for four particles](pics/4p_graph.svg 
"weighted undirected graph for four particles")

## Scope and limitations

The main idea is to solve this problem for at least 100 particles 
in less than 30 seconds on a single CPU. If this is not possible 
to guarantee the global minimum, the code tries to find a solution
*close enough* to it.
