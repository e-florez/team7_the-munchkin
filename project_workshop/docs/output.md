```
**************************************************
*     Do Research Like a Munchkin (Mar 2021)     *
*         The Workshop Research Project          *
*                                                *
* Minimize sum of pair distances for a system    *
* of M particles in a N-dimensional space        *
*                                                *
* by:                                            *
*   Sara Wirsing (),                             *
*   Julian Heuer (),                             *
*   Edison Florez (edisonffh@mail.com),          *
*   Gergely Juhasz ()                            *
**************************************************

--------------------------------------------------
- Reading Input file
--------------------------------------------------
  input file: input1.dat

--------------------------------------------------
- Coordinates
--------------------------------------------------
  system of 4 particles in a 1 dimensional space

[-10.  -1.  -2.   1.]

--------------------------------------------------
- Distance matrix
--------------------------------------------------
[[ 0.  9.  8. 11.]
 [ 0.  0.  1.  2.]
 [ 0.  0.  0.  3.]
 [ 0.  0.  0.  0.]]

  Time computing the distance matrix: 1.438e-03 sec

--------------------------------------------------
- Pairing closest neighbour
--------------------------------------------------
  Minimum sum of the pair distances: 10.0000
  for this pairs: [0, 2, 1, 3]

  Time computing closest neighbour approach: 3.22e-04 sec

--------------------------------------------------
- Brute force Approach
--------------------------------------------------
  for a system of 4 particles
  there are 3.00e+00 different ways to pair them
  global minimum sum of the pair distances: 10.0000
  for this pairs: 0-2-1-3

  Time computing brute force approach: 2.77e-04 sec

**************************************************
END of Execution
elapse time: 5.767e+00 sec
**************************************************
```