# Brute Force approach

Function to Generate every possible pair and computing the total distance from a set of *N* particles. We generate *K*
lists of *N/2* pairs, where

> ![\large K=\frac{N!}{2^{N/2}(N/2)!}](https://latex.codecogs.com/svg.latex?\large&space;K=\frac{N!}{2^{N/2}(N/2)!)

For instance, for a set of four particles in a one dimension

- p1: x = -10
- p2: x = -1
- p3: x = -2
- p4: x = 1

We could pair them in three different ways (*K=3*), avoiding the prefix *p* for particles

- [(1, 2), (3, 4)]
- [(1, 3), (2, 4)]
- [(1, 4), (2, 3)]

For each list we compute the sum of the Euclidean distance between those pairs.

- for [(1, 2), (3, 4)], sum of distances **12 units**
- for [(1, 3), (2, 4)], sum of distances **10 units**
- for [(1, 4), (2, 3)], sum of distances **12 units**

> **Warning!:** *K* is approx 650 millions for 20 particles

-----

Technical detail:

This function receives ONE inputs and returns TWO outputs. It uses `factorial` function from `math` package
(`from math import factorial`).

- Arguments:
    1. `d_matrix [NxN array]`: distance matrix for a system of N particles
    2. `create_dict [boolean]`: dictionary is only created by request, it could be too big in size
- Returns:
    1. `sum_of_all_possible_pairs [dict]`: a dictionary where keys are particle index paired and their total sum of each
       pair are their values
    2. `min_sum_of_pairs [float]`: global min for the total sum of pair distances