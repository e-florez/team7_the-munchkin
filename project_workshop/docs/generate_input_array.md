# Generate input array with a known solution

Function to generate coordinates for a *M* particles system in an *N*-dimensional space, where every coordinate `r` is generated according to `(r_i - r_j) = t`
for all the closest pairs (the distance for any other particle must be larger than `t`). 

As a result, the sum of the closest pairs is 

![\large Min(\text{sum pair distance}) = \frac{tM}{2}\sqrt{N}](https://latex.codecogs.com/svg.image?\large&space;Min(\text{sum&space;pair&space;distance})&space;=&space;\frac{tM}{2}\sqrt{N}) 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(eq. 1)

For instance:

For a system of four particles in a three-dimensional space, we could  generate a random input array 
of dimension (4x3) when the closest pairs are ONE unit away each other, and the next pair are TWO units away. Then, the array looks like  

```
[[3 3 3]
 [0 0 0]
 [4 4 4]
 [1 1 1]]
```

for four particles:

 - p0: (3, 3, 3)
 - p1: (0, 0, 0)
 - p2: (4, 4, 4)
 - p3: (1, 1, 1)

The minimum sum of pair distances means that the best pairing is `[1, 3, 0, 2]`. Where the particle one (p1) is paired with the particle three (p3), and particle zero (p0) is paired with the particle two (p2).


As a result, minimum sum of the pair distances is `3.4641` units (eq. 1)

-----

## Technical detail:

This function receives ONE inputs and returns ONE outputs. 

- Arguments:
    1. `num_particles [int]`: number of particles
    2. `dim_space [int]`: space dimension
    3. `min_distance [int]`: min distance for every coordinate between each pair (the closest)
    4. `next_pair [int]`: distance to the next pair (by default is twice the min distance)

- Returns:
    1. `generated_array [numpy arra]`: array with dimension (num_particles, dim_space)
    2. `list_of_closest_pairs [list (int)]`: list of the closest pair

### NOTE:

To avoid the trivial pairing solution (`[0, 1, 2, 3, ...]`), after generating all the requested pairs, we shuffle them using `random.sample` function and fixing a `seed = 123456` for reproducibility and testing purposes. 
