#Sum Over Distances Function

This function provides a sum over pairwise distances without double counting any particle. 
It is to be enhanced by further approximations. 

15.04.21: In this version, it searches for the smallest distance (not 0!) in a scipy distance matrix.
Deletes all entries related to the particles which are connected by this distance, and so on until the matrix 
is empty. All the smallest distances are added up. 