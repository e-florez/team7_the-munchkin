# Brute Force approach

The simplest and most expensive solution is to generate 
every possible pair and computing the total distance for
each of them. 

Then, from a set of *M* particles, we generate *K* 
permutation, where

> ![\large K=\frac{M!}{2^{M/2}(M/2)!}](https://latex.codecogs.com/svg.latex?\large&space;K=\frac{M!}{2^{M/2}(M/2)!)

Next, we search every permutation of cycles and returns 
the shortest one. Simple algorithm that guarantees the optimal
solution. This methodology has **O(M!)** efficiency. 
Therefore, it is not feasible more than 100 particles, 
regardless of how fast your for loops were 
(e.g., *K* is approx 650 millions permutations for 20 particles). 

For instance, for a set of four particles in a one dimension

- p1: x = -10
- p2: x = -1
- p3: x = -2
- p4: x = 1

We could pair them in three different ways (*K=3*), avoiding the prefix *p* for particles

- [(1, 2), (3, 4)]
- [(1, 3), (2, 4)]
- [(1, 4), (2, 3)]

For each list we compute the sum of the Euclidean distance between those pairs.

- for [(1, 2), (3, 4)], sum of distances **12 units**
- for [(1, 3), (2, 4)], sum of distances **10 units**
- for [(1, 4), (2, 3)], sum of distances **12 units**

# The Nearest Neighbour Distances

We use a *linear search* computing the distance from a certain
particle to every other particles, keeping track of 
***the nearest so far***. After matching the first nearest 
neighbour, we start the search again to find the next closest pair
and so on. This approach provides a sum over pairwise distances
without double counting any particle with a computational 
complexity of **O(NM^2)**, for a set of *M* particles in an *N*-space. 

##  Greedy Search of the Nearest Neighbour

To improve the *linear search*, by removing the initial condition
dependence. 

We choose the first particle and find its nearest neighbour, 
then the second one until all particles have been exhausted. 
In the end, we compute the sum of pair distances.

Next, we restart the procedure by choosing the second particle
and find its nearest neighbour, then we move to the next particle
and so on. We keep track of ***minimum sum of pair distances
so far*** to return the shortest path.

This approach has a computational complexity of **O(NM^3)**, 
for a set of *M* particles in an *N*-space.


### NOTE: 
In those cases (only in the brute force one), we can't 
guarantee to return the actual nearest neighbor in every case,
in return for improved speed or memory savings. Often such 
an algorithm will find the nearest neighbor 
in a majority of cases, but this depends strongly on the
dataset being queried.