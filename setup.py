from setuptools import setup

setup(name='minimizing the sum of pair distances',
      version='1.0.0',
      packages=['project_workshop'],
      entry_points={'console_scripts': ['project_workshop = project_workshop.__main__:main']}
      )
